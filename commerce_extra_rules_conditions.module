<?php
/**
 * @file
 * Defines Rules conditions for checking if a line item's product has a
 * specified taxonomy term and for calculating the total quantity of
 * products in an order that has a specified taxonomy term.
 */

/**
 * Implements hook_rules_conditions_info()
 */
function commerce_extra_rules_conditions_rules_condition_info() {
  //Define the condition for checking if a line item has a product with a
  //specified term
  $conditions['commerce_extra_rules_conditions_rules_condition_has_term'] = array(
    'group' => t('Commerce Line Item'),
    'label' => t('Line item product has term'),
    'parameter' => array(
      'line_item' => array(
        'type' => 'commerce_line_item', 
        'label' => t('Commere Line Item'),
      ),
      'field_name' => array(
        'type' => 'text',
        'label' => t('Term Reference Field'),
        'description' => t('The machine-name of the expected product\'s term reference field'),
        'options list' => 'commerce_extra_rules_conditions_term_fields_options_list',
        'restriction' => 'input',
      ),
      'term_id' => array(
        'type' => 'integer',
        'label' => t('Taxonomy Term'),
        'description' => t('The term being checked for'),
        'restriction' => 'input',
      ),
    ),
  );
  //Define the condition for comparing the quantity of products with a
  //specified term in an order to a value
  $conditions['commerce_extra_rules_conditions_compare_termed_product_quantity'] = array(
    'group' => t('Commerce Order'),
    'label' => t('Term-based product quantity comparison'),
    'parameter' => array(
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('The order whose product line item quantities should be totalled. If the specified order does not exist, the comparison will act as if it is against a quantity of 0.'),
      ),
      'field_name' => array(
        'type' => 'text',
        'label' => t('Term Reference Field'),
        'description' => t('The machine-name of the expected product\'s term reference field'),
        'options list' => 'commerce_extra_rules_conditions_term_fields_options_list',
        'restriction' => 'input',
      ),
      'term_id' => array(
        'type' => 'integer',
        'label' => t('Taxonomy Term'),
        'description' => t('The term being checked for'),
        'restriction' => 'input',
      ),
      'operator' => array(
        'type' => 'text',
        'label' => t('Operator'),
        'description' => t('The comparison operator to use against the total number of products matching the term on the order.'),
        'default value' => '>=',
        'options list' => 'commerce_numeric_comparison_operator_options_list',
        'restriction' => 'input',
      ),
      'value' => array(
        'type' => 'integer',
        'label' => t('Quantity'),
        'default value' => 1,
        'description' => t('The value to compare against the total quantity of products matching the term on the order.'),
      ),
    ),
  );

  return $conditions;
}
/**
 * Rule condition for checking if a line item has a product with a certain term
 * id
 * 
 * @param object $line_item
 *   A commerce_line_item containing the product being checked
 * @param string $field_name
 *   A string containing the machine name of a Taxonomy reference field
 * @param integer $term_id
 *   An integer corresponding to a Taxonomy term id
 * 
 * @return
 *   TRUE if the product has the term applied to it on the field $field_name
 *   Otherwise FALSE
 */
function commerce_extra_rules_conditions_rules_condition_has_term($line_item, $field_name, $term_id) {
  $field_name = trim($field_name);
  return commerce_extra_rules_conditions_has_term($line_item, $field_name, $term_id);
}

/**
 * Calculates the quantity of products in an order that have the term $term_id
 * and compares it to a given value
 * 
 * @param object $order
 *   A commerce_order containing the products being checked
 * @param string $field_name
 *   A string containing the machine name of a Taxonomy reference field
 * @param integer $term_id
 *   An integer corresponding to a Taxonomy term id
 * @param string $operator
 *   A string containing the operator used comparing the calculated quantity to 
 *   $value
 * @param integer $value
 *   An integer to compare to the quantity of products containing $term_id in
 *   the $field_name field
 * 
 * @return
 *   The result of evaluating the calculated quantity against $value with the
 *   specified operator.
 */
function commerce_extra_rules_conditions_compare_termed_product_quantity($order, $field_name, $term_id, $operator, $value) {
  $quantity = 0;
  $field_name = trim($field_name);
  
  if (!empty($order)) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    $line_items = $wrapper->commerce_line_items->value();
    if (!empty($line_items)) {
      //Holds line items that have products with $term_id
      $termed_line_items = array();
      foreach($line_items as $line_item) {
        if(commerce_extra_rules_conditions_has_term($line_item, $field_name, $term_id)) {
          $termed_line_items[] = $line_item;
        }
      }
      if(!empty($termed_line_items)) {
        $quantity = commerce_line_items_quantity($termed_line_items, commerce_product_line_item_types());
      }
    }
  }

  // Make a quantity comparison based on the operator.
  switch ($operator) {
    case '<':
      return $quantity < $value;
    case '<=':
      return $quantity <= $value;
    case '=':
      return $quantity == $value;
    case '>=':
      return $quantity >= $value;
    case '>':
      return $quantity > $value;
  }

  return FALSE;
}

/**
 * Check if a line item has a product with a certain term id
 * 
 * @param object $line_item
 *   A commerce_line_item containing the product being checked
 * @param string $field_name
 *   A string containing the machine name of a Taxonomy reference field
 * @param integer $term_id
 *   An integer corresponding to a Taxonomy term id
 * 
 * @return
 *   TRUE if the product has the term applied to it on the field $field_name
 *   Otherwise FALSE
 */
function commerce_extra_rules_conditions_has_term($line_item, $field_name, $term_id) {
  if(!empty($line_item)){
    $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    $product = $wrapper->commerce_product->value();
    if(isset($product->$field_name)) {
      $product_terms = $wrapper->commerce_product->$field_name->value();
    }
    if(!empty($product_terms)) {
      //If ther term reference field is set to allow more than one term
      //$product_terms will be an array
      if(is_array($product_terms)) {
        foreach($product_terms as $product_term){
          if($product_term->tid == $term_id)
            return TRUE;
        }
      }
      else{
        if($product_terms->tid == $term_id) {
          return TRUE;
        }
      }
    }
  }

  return FALSE;
}

/**
 * Returns an array of machine_names for taxonomy term reference fields
 */
function commerce_extra_rules_conditions_term_fields_options_list() {
  $field_options_list = array();
  $fields = field_read_fields(array('type' => 'taxonomy_term_reference'));
  if(!empty($fields)){
    foreach($fields as $key => $value) {
      $field_options_list[$key] = $key;
    }
  }

  return $field_options_list;
}

/**
 * Alters the form for commerce_extra_rules_conditions_rules_condition_has_term
 * so that we can require the term reference field be selected first so that
 * the term_id list can be populated.
 */ 
function commerce_extra_rules_conditions_rules_condition_has_term_form_alter(&$form, &$form_state, $options, RulesAbstractPlugin $element) {
  if (!empty($options['init']) && !isset($form_state['rules_element_step'])) {
    unset($form['parameter']['term_id']);
    $form_state['rules_element_step'] = 1;
    $form['negate']['#access'] = FALSE;
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Continue'),
      '#limit_validation_errors' => array(array('parameter', 'list')),
      '#submit' => array('rules_form_submit_rebuild'),
    );
  }
  else {
    // Change the list parameter to be not editable any more.
    $form['parameter']['field_name']['settings']['#access'] = FALSE;
    $form['parameter']['field_name']['info'] = array(
      '#prefix' => '<p>',
      '#markup' => t('<strong>Selected term field:</strong> %selector', array('%selector' => $element->settings['field_name'])),
      '#suffix' => '</p>',
    );
    $form['parameter']['term_id']['settings']['term_id']['#type'] = 'select';
    $form['parameter']['term_id']['settings']['term_id']['#options'] = commerce_extra_rules_conditions_get_terms_list($element->settings['field_name']);
  }
}

/**
 * Alters the form for 
 * commerce_extra_rules_conditions_compare_termed_product_quantity so that we
 * can require the term reference field be selected first so that the term_id
 * list can be populated.
 */
function commerce_extra_rules_conditions_compare_termed_product_quantity_form_alter(&$form, &$form_state, $options, RulesAbstractPlugin $element) {
  if (!empty($options['init']) && !isset($form_state['rules_element_step'])) {
    unset($form['parameter']['term_id'], $form['parameter']['operator'], $form['parameter']['value']);
    $form_state['rules_element_step'] = 1;
    $form['negate']['#access'] = FALSE;
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Continue'),
      '#limit_validation_errors' => array(array('parameter', 'list')),
      '#submit' => array('rules_form_submit_rebuild'),
    );
  }
  else {
    // Change the list parameter to be not editable any more.
    $form['parameter']['field_name']['settings']['#access'] = FALSE;
    $form['parameter']['field_name']['info'] = array(
      '#prefix' => '<p>',
      '#markup' => t('<strong>Selected term field:</strong> %selector', array('%selector' => $element->settings['field_name'])),
      '#suffix' => '</p>',
    );
    $form['parameter']['term_id']['settings']['term_id']['#type'] = 'select';
    $form['parameter']['term_id']['settings']['term_id']['#options'] = commerce_extra_rules_conditions_get_terms_list($element->settings['field_name']);
  }
}

/**
 * Takes the machine name of a Taxonomy reference field and retrieves the terms
 * for the associated vocabulary.
 * 
 * @param string $field_name
 *   A string containing the machine name of a Taxonomy reference field.
 * 
 * @return
 *   An array containing the term names of the vocabulary tied to $field_name
 *   prefixed with a certain number of dashes(-) corresponding to the depth of
 *   the term. Term names are keyed by term id.
 */
function commerce_extra_rules_conditions_get_terms_list($field_name) {
  $field = field_read_field($field_name);
  $vocabulary = taxonomy_vocabulary_machine_name_load($field['settings']['allowed_values'][0]['vocabulary']);
  $terms = taxonomy_get_tree($vocabulary->vid);
  $term_list = array();
  if(!empty($terms)) {
    foreach($terms as $term) {
      $term_list[$term->tid] = str_repeat('-', $term->depth) . $term->name;
    }
  }

  return $term_list;
}
